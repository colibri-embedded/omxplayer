cmake_minimum_required (VERSION 3.1)

project(omxplayer VERSION 1.0.0)

# detect operating system and host processor
message(STATUS "We are on a ${CMAKE_SYSTEM_NAME} system")

set(INSTALL_BIN_DIR "${CMAKE_INSTALL_PREFIX}/bin" CACHE PATH "Installation directory for executables")
set(INSTALL_LIB_DIR "${CMAKE_INSTALL_PREFIX}/lib" CACHE PATH  "Installation directory for libraries")
# set(INSTALL_INC_DIR "${CMAKE_INSTALL_PREFIX}/include/dbusxx" CACHE PATH "Installation directory for headers")
# set(INSTALL_PKGCONFIG_DIR "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" CACHE PATH "Installation directory for pkgconfig (.pc) files")
# set(INSTALL_CMAKE_DIR "${CMAKE_INSTALL_PREFIX}/lib/cmake/libdbusxx" CACHE PATH  "Installation directory for libraries")
# set(3RDPARTY_SOURCE_DIR "${PROJECT_SOURCE_DIR}/3rdparty")
# set(INCLUDE_DIR "${PROJECT_SOURCE_DIR}/include")

# Skip adding rpath when linking a shared library
set (CMAKE_SKIP_RPATH NO)
# Skip adding rpath when installing the targets
set (CMAKE_SKIP_INSTALL_RPATH YES)
# Add local cmake modules
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/modules")

# add_compile_options(-Wall -Wextra -pedantic)

include_directories("${PROJECT_SOURCE_DIR}")
include_directories("${PROJECT_BINARY_DIR}")

include(GNUInstallDirs)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
find_package(DBus REQUIRED)
find_package(Freetype REQUIRED)
find_package(OMX REQUIRED)
find_package(ZLIB REQUIRED)
find_package(PCRE REQUIRED) 

find_package(PkgConfig REQUIRED) 
pkg_check_modules(BRCM_EGL REQUIRED brcmegl)
pkg_check_modules(ALSA REQUIRED alsa)

add_custom_command(
    OUTPUT  help.h
    COMMAND sh ${PROJECT_SOURCE_DIR}/gen_help.sh ${PROJECT_SOURCE_DIR}/README.md ${PROJECT_BINARY_DIR}/help.h
    DEPENDS README.md
)

add_custom_command(
    OUTPUT  keys.h
    COMMAND sh ${PROJECT_SOURCE_DIR}/gen_keys.sh ${PROJECT_SOURCE_DIR}/README.md ${PROJECT_BINARY_DIR}/keys.h
    DEPENDS README.md
)

set(SOURCES
    ${PROJECT_SOURCE_DIR}/linux/XMemUtils.cpp
    ${PROJECT_SOURCE_DIR}/linux/OMXAlsa.cpp
    ${PROJECT_SOURCE_DIR}/utils/log.cpp
    ${PROJECT_SOURCE_DIR}/DynamicDll.cpp
    ${PROJECT_SOURCE_DIR}/utils/PCMRemap.cpp
    ${PROJECT_SOURCE_DIR}/utils/RegExp.cpp
    ${PROJECT_SOURCE_DIR}/OMXSubtitleTagSami.cpp
    ${PROJECT_SOURCE_DIR}/OMXOverlayCodecText.cpp
    ${PROJECT_SOURCE_DIR}/BitstreamConverter.cpp
    ${PROJECT_SOURCE_DIR}/linux/RBP.cpp
    ${PROJECT_SOURCE_DIR}/OMXThread.cpp
    ${PROJECT_SOURCE_DIR}/OMXReader.cpp
    ${PROJECT_SOURCE_DIR}/OMXStreamInfo.cpp
    ${PROJECT_SOURCE_DIR}/OMXAudioCodecOMX.cpp
    ${PROJECT_SOURCE_DIR}/OMXCore.cpp
    ${PROJECT_SOURCE_DIR}/OMXVideo.cpp
    ${PROJECT_SOURCE_DIR}/OMXAudio.cpp
    ${PROJECT_SOURCE_DIR}/OMXClock.cpp
    ${PROJECT_SOURCE_DIR}/File.cpp
    ${PROJECT_SOURCE_DIR}/OMXPlayerVideo.cpp
    ${PROJECT_SOURCE_DIR}/OMXPlayerAudio.cpp
    ${PROJECT_SOURCE_DIR}/OMXPlayerSubtitles.cpp
    ${PROJECT_SOURCE_DIR}/SubtitleRenderer.cpp
    ${PROJECT_SOURCE_DIR}/Unicode.cpp
    ${PROJECT_SOURCE_DIR}/Srt.cpp
    ${PROJECT_SOURCE_DIR}/KeyConfig.cpp
    ${PROJECT_SOURCE_DIR}/Keyboard.cpp
    ${PROJECT_SOURCE_DIR}/revision.cpp 
)

set(APP_SOURCES
    ${SOURCES}
    omxplayer2.cpp
    # ${PROJECT_SOURCE_DIR}/OMXControl.cpp
    ${PROJECT_BINARY_DIR}/help.h
    ${PROJECT_BINARY_DIR}/keys.h
)

add_definitions(
    -D__STDC_CONSTANT_MACROS
    -D__STDC_LIMIT_MACROS
    -D_REENTRANT
    -D_LARGEFILE64_SOURCE 
    -D_FILE_OFFSET_BITS=64 
    -D__VIDEOCORE4__
    -DTARGET_POSIX 
    -DTARGET_LINUX 
    -DTARGET_RASPBERRY_PI  
)

add_definitions(
    -DPIC
    -DHAVE_CMAKE_CONFIG
    -DHAVE_LIBAVCODEC_AVCODEC_H 
    -DHAVE_LIBAVUTIL_OPT_H 
    -DHAVE_LIBAVUTIL_MEM_H 
    -DHAVE_LIBAVUTIL_AVUTIL_H 
    -DHAVE_LIBAVFORMAT_AVFORMAT_H 
    -DHAVE_LIBAVFILTER_AVFILTER_H 
    -DHAVE_LIBSWRESAMPLE_SWRESAMPLE_H 
    -DHAVE_OMXLIB
    -DOMX 
    -DOMX_SKIP64BIT 
    -DUSE_EXTERNAL_OMX 
    -DUSE_EXTERNAL_FFMPEG  
    -DUSE_EXTERNAL_LIBBCM_HOST
)

add_compile_options(
    -mfloat-abi=hard 
    -mcpu=arm1176jzf-s
    -mabi=aapcs-linux 
    -mtune=arm1176jzf-s 
    -mfpu=vfp  
)

add_compile_options(
    -Wall 
    -fPIC
    -ftree-vectorize
    -fomit-frame-pointer 
    -pipe 
    -Wno-psabi 
    -U_FORTIFY_SOURCE 
    -Wno-deprecated
)

add_executable(omxplayer ${APP_SOURCES})
target_include_directories (omxplayer PUBLIC 
    ${PROJECT_SOURCE_DIR} 
    ${PROJECT_SOURCE_DIR}/linux 
    ${DBUS_INCLUDE_DIRS}
    ${FREETYPE_INCLUDE_DIRS}
    ${BRCM_EGL_INCLUDE_DIRS}
    ${OMX_INCLUDE_DIRS}
    ${FFMPEG_INCLUDE_DIRS}
    ${PCRE_INCLUDE_DIRS}
    ${SWRESAMPLE_INCLUDE_DIRS}
    ${ALSA_INCLUDE_DIRS}
)
set_property(TARGET omxplayer PROPERTY CXX_STANDARD 11)
set_property(TARGET omxplayer PROPERTY CXX_STANDARD_REQUIRED ON)

target_link_libraries(omxplayer 
    Threads::Threads 
    ${DBUS_LIBRARIES} 
    ${FREETYPE_LIBRARIES}
    ${BRCM_EGL_LIBRARIES}
    ${OMX_LIBRARIES}
    ${ZLIB_LIBRARIES}
    ${FFMPEG_LIBRARIES}
    ${PCRE_LIBRARIES}
    ${SWRESAMPLE_LIBRARIES}
    ${ALSA_LIBRARIES}
)

add_subdirectory(cmake)
add_subdirectory(lib)

###
