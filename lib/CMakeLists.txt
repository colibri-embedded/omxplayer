set(SOURCES
    ${PROJECT_SOURCE_DIR}/linux/XMemUtils.cpp
    ${PROJECT_SOURCE_DIR}/linux/OMXAlsa.cpp
    ${PROJECT_SOURCE_DIR}/utils/log.cpp
    ${PROJECT_SOURCE_DIR}/DynamicDll.cpp
    ${PROJECT_SOURCE_DIR}/utils/PCMRemap.cpp
    ${PROJECT_SOURCE_DIR}/utils/RegExp.cpp
    ${PROJECT_SOURCE_DIR}/OMXSubtitleTagSami.cpp
    ${PROJECT_SOURCE_DIR}/OMXOverlayCodecText.cpp
    ${PROJECT_SOURCE_DIR}/BitstreamConverter.cpp
    ${PROJECT_SOURCE_DIR}/linux/RBP.cpp
    ${PROJECT_SOURCE_DIR}/OMXThread.cpp
    ${PROJECT_SOURCE_DIR}/OMXReader.cpp
    ${PROJECT_SOURCE_DIR}/OMXStreamInfo.cpp
    ${PROJECT_SOURCE_DIR}/OMXAudioCodecOMX.cpp
    ${PROJECT_SOURCE_DIR}/OMXCore.cpp
    ${PROJECT_SOURCE_DIR}/OMXVideo.cpp
    ${PROJECT_SOURCE_DIR}/OMXAudio.cpp
    ${PROJECT_SOURCE_DIR}/OMXClock.cpp
    ${PROJECT_SOURCE_DIR}/File.cpp
    ${PROJECT_SOURCE_DIR}/OMXPlayerVideo.cpp
    ${PROJECT_SOURCE_DIR}/OMXPlayerAudio.cpp
    ${PROJECT_SOURCE_DIR}/OMXPlayerSubtitles.cpp
    ${PROJECT_SOURCE_DIR}/SubtitleRenderer.cpp
    ${PROJECT_SOURCE_DIR}/Unicode.cpp
    ${PROJECT_SOURCE_DIR}/Srt.cpp
    ${PROJECT_SOURCE_DIR}/KeyConfig.cpp
    ${PROJECT_SOURCE_DIR}/OMXControl.cpp
    ${PROJECT_SOURCE_DIR}/Keyboard.cpp
    ${PROJECT_SOURCE_DIR}/revision.cpp

    OMXPlayer.cpp
    OMXPlayerImpl.cpp
)

add_library(omxplayer-shared SHARED ${SOURCES})
# target_compile_options(omxplayer-shared PUBLIC -Wno-deprecated)
target_include_directories (omxplayer-shared PUBLIC 
    ${PROJECT_SOURCE_DIR} 
    ${PROJECT_SOURCE_DIR}/linux 
    ${DBUS_INCLUDE_DIRS}
    ${FREETYPE_INCLUDE_DIRS}
    ${BRCM_EGL_INCLUDE_DIRS}
    ${OMX_INCLUDE_DIRS}
    ${FFMPEG_INCLUDE_DIRS}
    ${PCRE_INCLUDE_DIRS}
    ${SWRESAMPLE_INCLUDE_DIRS}
    ${ALSA_INCLUDE_DIRS}
)
set_target_properties(omxplayer-shared PROPERTIES VERSION 1.0.0)
set_target_properties(omxplayer-shared PROPERTIES OUTPUT_NAME omxplayer CLEAN_DIRECT_OUTPUT 1)

set(APP_SOURCES
    omxtestapp.cpp
)

add_executable(omxtestapp ${APP_SOURCES})
target_include_directories (omxtestapp PUBLIC 
    ${PROJECT_SOURCE_DIR} 
    ${PROJECT_SOURCE_DIR}/linux 
    ${DBUS_INCLUDE_DIRS}
    ${FREETYPE_INCLUDE_DIRS}
    ${BRCM_EGL_INCLUDE_DIRS}
    ${OMX_INCLUDE_DIRS}
    ${FFMPEG_INCLUDE_DIRS}
    ${PCRE_INCLUDE_DIRS}
    ${SWRESAMPLE_INCLUDE_DIRS}
    ${ALSA_INCLUDE_DIRS}
)
set_property(TARGET omxtestapp PROPERTY CXX_STANDARD 11)
set_property(TARGET omxtestapp PROPERTY CXX_STANDARD_REQUIRED ON)

target_link_libraries(omxtestapp 
    omxplayer-shared
    Threads::Threads 
    ${DBUS_LIBRARIES} 
    ${FREETYPE_LIBRARIES}
    ${BRCM_EGL_LIBRARIES}
    ${OMX_LIBRARIES}
    ${ZLIB_LIBRARIES}
    ${FFMPEG_LIBRARIES}
    ${PCRE_LIBRARIES}
    ${SWRESAMPLE_LIBRARIES}
    ${ALSA_LIBRARIES}
)