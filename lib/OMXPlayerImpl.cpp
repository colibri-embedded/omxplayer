#include "OMXPlayerImpl.h"

#include <iostream>

#define TRICKPLAY(speed) (speed < 0 || speed > 4 * DVD_PLAYSPEED_NORMAL)

enum
{
    ERROR = -1,
    SUCCESS,
    ONEBYTE
};

static void CallbackTvServiceCallback(void *userdata, uint32_t reason, uint32_t param1, uint32_t param2)
{
    sem_t *tv_synced = (sem_t *)userdata;
    switch (reason)
    {
    case VC_HDMI_UNPLUGGED:
        break;
    case VC_HDMI_STANDBY:
        break;
    case VC_SDTV_NTSC:
    case VC_SDTV_PAL:
    case VC_HDMI_HDMI:
    case VC_HDMI_DVI:
        // Signal we are ready now
        sem_post(tv_synced);
        break;
    default:
        break;
    }
}

OMXPlayerImpl::OMXPlayerImpl()
{
    g_RBP.Initialize();
    g_OMX.Initialize();
    m_av_clock = new OMXClock();

    FlushStreams(DVD_NOPTS_VALUE);

    Create();
}

OMXPlayerImpl::~OMXPlayerImpl()
{
    Close();

    m_av_clock->OMXDeinitialize();
    if (m_av_clock)
        delete m_av_clock;

    g_OMX.Deinitialize();
    g_RBP.Deinitialize();
}

bool OMXPlayerImpl::Open()
{
    return false;
}

bool OMXPlayerImpl::Close()
{
    if (ThreadHandle())
    {
        StopThread();
    }
    return false;
}

void OMXPlayerImpl::FlushStreams(double pts)
{
    m_av_clock->OMXStop();
    m_av_clock->OMXPause();

    if (m_has_video)
        m_player_video.Flush();

    if (m_has_audio)
        m_player_audio.Flush();

    if (pts != DVD_NOPTS_VALUE)
        m_av_clock->OMXMediaTime(pts);

    // if(m_has_subtitle)
    //   m_player_subtitles.Flush();

    if (m_omx_pkt)
    {
        m_omx_reader.FreePacket(m_omx_pkt);
        m_omx_pkt = NULL;
    }
}

void OMXPlayerImpl::SetWindowRect(int x1, int y1, int width, int height)
{
    m_config_video.dst_rect.x1 = x1;
    m_config_video.dst_rect.y1 = y1;
    m_config_video.dst_rect.x2 = x1+width;
    m_config_video.dst_rect.y2 = y1+height;
}

void OMXPlayerImpl::SetCropRect(int x1, int y1, int width, int height)
{
    m_config_video.src_rect.x1 = x1;
    m_config_video.src_rect.y1 = y1;
    m_config_video.src_rect.x2 = x1 + width;
    m_config_video.src_rect.y2 = y1 + height;
}

void OMXPlayerImpl::SetLayer(unsigned layer)
{
    // m_player_video.SetLayer(layer);
    m_config_video.layer = layer;
}

void OMXPlayerImpl::SetAlpha(unsigned alpha)
{
    // m_player_video.SetAlpha(alpha);
    m_config_video.alpha = alpha;
}

void OMXPlayerImpl::SetSpeed(int iSpeed)
{
    if (!m_av_clock)
        return;

    m_omx_reader.SetSpeed(iSpeed);

    // flush when in trickplay mode
    if (TRICKPLAY(iSpeed) || TRICKPLAY(m_av_clock->OMXPlaySpeed()))
        FlushStreams(DVD_NOPTS_VALUE);

    m_av_clock->OMXSetSpeed(iSpeed);
    m_av_clock->OMXSetSpeed(iSpeed, true, true);
}

float OMXPlayerImpl::GetDisplayAspectRatio(HDMI_ASPECT_T aspect)
{
    float display_aspect;
    switch (aspect)
    {
    case HDMI_ASPECT_4_3:
        display_aspect = 4.0 / 3.0;
        break;
    case HDMI_ASPECT_14_9:
        display_aspect = 14.0 / 9.0;
        break;
    case HDMI_ASPECT_16_9:
        display_aspect = 16.0 / 9.0;
        break;
    case HDMI_ASPECT_5_4:
        display_aspect = 5.0 / 4.0;
        break;
    case HDMI_ASPECT_16_10:
        display_aspect = 16.0 / 10.0;
        break;
    case HDMI_ASPECT_15_9:
        display_aspect = 15.0 / 9.0;
        break;
    case HDMI_ASPECT_64_27:
        display_aspect = 64.0 / 27.0;
        break;
    default:
        display_aspect = 16.0 / 9.0;
        break;
    }
    return display_aspect;
}

float OMXPlayerImpl::GetDisplayAspectRatio(SDTV_ASPECT_T aspect)
{
    float display_aspect;
    switch (aspect)
    {
    case SDTV_ASPECT_4_3:
        display_aspect = 4.0 / 3.0;
        break;
    case SDTV_ASPECT_14_9:
        display_aspect = 14.0 / 9.0;
        break;
    case SDTV_ASPECT_16_9:
        display_aspect = 16.0 / 9.0;
        break;
    default:
        display_aspect = 4.0 / 3.0;
        break;
    }
    return display_aspect;
}

void OMXPlayerImpl::SetVideoMode(int width, int height, int fpsrate, int fpsscale, FORMAT_3D_T is3d)
{
    int32_t num_modes = 0;
    int i;
    HDMI_RES_GROUP_T prefer_group;
    HDMI_RES_GROUP_T group = HDMI_RES_GROUP_CEA;
    float fps = 60.0f; // better to force to higher rate if no information is known
    uint32_t prefer_mode;

    if (fpsrate && fpsscale)
        fps = DVD_TIME_BASE / OMXReader::NormalizeFrameduration((double)DVD_TIME_BASE * fpsscale / fpsrate);

    //Supported HDMI CEA/DMT resolutions, preferred resolution will be returned
    TV_SUPPORTED_MODE_NEW_T *supported_modes = NULL;
    // query the number of modes first
    int max_supported_modes = m_BcmHost.vc_tv_hdmi_get_supported_modes_new(group, NULL, 0, &prefer_group, &prefer_mode);

    if (max_supported_modes > 0)
        supported_modes = new TV_SUPPORTED_MODE_NEW_T[max_supported_modes];

    if (supported_modes)
    {
        num_modes = m_BcmHost.vc_tv_hdmi_get_supported_modes_new(group,
                                                                 supported_modes, max_supported_modes, &prefer_group, &prefer_mode);

        // if (m_gen_log)
        // {
        //     CLog::Log(LOGDEBUG, "EGL get supported modes (%d) = %d, prefer_group=%x, prefer_mode=%x\n",
        //               group, num_modes, prefer_group, prefer_mode);
        // }
    }

    TV_SUPPORTED_MODE_NEW_T *tv_found = NULL;

    if (num_modes > 0 && prefer_group != HDMI_RES_GROUP_INVALID)
    {
        uint32_t best_score = 1 << 30;
        uint32_t scan_mode = m_NativeDeinterlace;

        for (i = 0; i < num_modes; i++)
        {
            TV_SUPPORTED_MODE_NEW_T *tv = supported_modes + i;
            uint32_t score = 0;
            uint32_t w = tv->width;
            uint32_t h = tv->height;
            uint32_t r = tv->frame_rate;

            /* Check if frame rate match (equal or exact multiple) */
            if (fabs(r - 1.0f * fps) / fps < 0.002f)
                score += 0;
            else if (fabs(r - 2.0f * fps) / fps < 0.002f)
                score += 1 << 8;
            else
                score += (1 << 16) + (1 << 20) / r; // bad - but prefer higher framerate

            /* Check size too, only choose, bigger resolutions */
            if (width && height)
            {
                /* cost of too small a resolution is high */
                score += max((int)(width - w), 0) * (1 << 16);
                score += max((int)(height - h), 0) * (1 << 16);
                /* cost of too high a resolution is lower */
                score += max((int)(w - width), 0) * (1 << 4);
                score += max((int)(h - height), 0) * (1 << 4);
            }

            // native is good
            if (!tv->native)
                score += 1 << 16;

            // interlace is bad
            if (scan_mode != tv->scan_mode)
                score += (1 << 16);

            // wanting 3D but not getting it is a negative
            if (is3d == CONF_FLAGS_FORMAT_SBS && !(tv->struct_3d_mask & HDMI_3D_STRUCT_SIDE_BY_SIDE_HALF_HORIZONTAL))
                score += 1 << 18;
            if (is3d == CONF_FLAGS_FORMAT_TB && !(tv->struct_3d_mask & HDMI_3D_STRUCT_TOP_AND_BOTTOM))
                score += 1 << 18;
            if (is3d == CONF_FLAGS_FORMAT_FP && !(tv->struct_3d_mask & HDMI_3D_STRUCT_FRAME_PACKING))
                score += 1 << 18;

            // prefer square pixels modes
            float par = GetDisplayAspectRatio((HDMI_ASPECT_T)tv->aspect_ratio) * (float)tv->height / (float)tv->width;
            score += fabs(par - 1.0f) * (1 << 12);

            /*printf("mode %dx%d@%d %s%s:%x par=%.2f score=%d\n", tv->width, tv->height, 
             tv->frame_rate, tv->native?"N":"", tv->scan_mode?"I":"", tv->code, par, score);*/

            if (score < best_score)
            {
                tv_found = tv;
                best_score = score;
            }
        }
    }

    if (tv_found)
    {
        char response[80];
        printf("Output mode %d: %dx%d@%d %s%s:%x\n", tv_found->code, tv_found->width, tv_found->height,
               tv_found->frame_rate, tv_found->native ? "N" : "", tv_found->scan_mode ? "I" : "", tv_found->code);
        if (m_NativeDeinterlace && tv_found->scan_mode)
            vc_gencmd(response, sizeof response, "hvs_update_fields %d", 1);

        // if we are closer to ntsc version of framerate, let gpu know
        int ifps = (int)(fps + 0.5f);
        bool ntsc_freq = fabs(fps * 1001.0f / 1000.0f - ifps) < fabs(fps - ifps);

        /* inform TV of ntsc setting */
        HDMI_PROPERTY_PARAM_T property;
        property.property = HDMI_PROPERTY_PIXEL_CLOCK_TYPE;
        property.param1 = ntsc_freq ? HDMI_PIXEL_CLOCK_TYPE_NTSC : HDMI_PIXEL_CLOCK_TYPE_PAL;
        property.param2 = 0;

        /* inform TV of any 3D settings. Note this property just applies to next hdmi mode change, so no need to call for 2D modes */
        property.property = HDMI_PROPERTY_3D_STRUCTURE;
        property.param1 = HDMI_3D_FORMAT_NONE;
        property.param2 = 0;
        if (is3d != CONF_FLAGS_FORMAT_NONE)
        {
            if (is3d == CONF_FLAGS_FORMAT_SBS && tv_found->struct_3d_mask & HDMI_3D_STRUCT_SIDE_BY_SIDE_HALF_HORIZONTAL)
                property.param1 = HDMI_3D_FORMAT_SBS_HALF;
            else if (is3d == CONF_FLAGS_FORMAT_TB && tv_found->struct_3d_mask & HDMI_3D_STRUCT_TOP_AND_BOTTOM)
                property.param1 = HDMI_3D_FORMAT_TB_HALF;
            else if (is3d == CONF_FLAGS_FORMAT_FP && tv_found->struct_3d_mask & HDMI_3D_STRUCT_FRAME_PACKING)
                property.param1 = HDMI_3D_FORMAT_FRAME_PACKING;
            m_BcmHost.vc_tv_hdmi_set_property(&property);
        }

        printf("ntsc_freq:%d %s\n", ntsc_freq, property.param1 == HDMI_3D_FORMAT_SBS_HALF ? "3DSBS" : property.param1 == HDMI_3D_FORMAT_TB_HALF ? "3DTB" : property.param1 == HDMI_3D_FORMAT_FRAME_PACKING ? "3DFP" : "");
        sem_t tv_synced;
        sem_init(&tv_synced, 0, 0);
        m_BcmHost.vc_tv_register_callback(CallbackTvServiceCallback, &tv_synced);
        int success = m_BcmHost.vc_tv_hdmi_power_on_explicit_new(HDMI_MODE_HDMI, (HDMI_RES_GROUP_T)group, tv_found->code);
        if (success == 0)
            sem_wait(&tv_synced);
        m_BcmHost.vc_tv_unregister_callback(CallbackTvServiceCallback);
        sem_destroy(&tv_synced);
    }
    if (supported_modes)
        delete[] supported_modes;
}

bool OMXPlayerImpl::Exists(const std::string &path)
{
    struct stat buf;
    auto error = stat(path.c_str(), &buf);
    return !error || errno != ENOENT;
}

bool OMXPlayerImpl::IsURL(const std::string &str)
{
    auto result = str.find("://");
    if (result == std::string::npos || result == 0)
        return false;

    for (size_t i = 0; i < result; ++i)
    {
        if (!isalpha(str[i]))
            return false;
    }
    return true;
}

bool OMXPlayerImpl::IsPipe(const std::string &str)
{
    if (str.compare(0, 5, "pipe:") == 0)
        return true;
    return false;
}

int OMXPlayerImpl::GetGPUMemory(void)
{
    char response[80] = "";
    int gpu_mem = 0;
    if (vc_gencmd(response, sizeof response, "get_mem gpu") == 0)
        vc_gencmd_number_property(response, "gpu", &gpu_mem);
    return gpu_mem;
}

void OMXPlayerImpl::SetBackground(uint32_t rgba)
{
    // if alpha is fully transparent then background has no effect
    if (!(rgba & 0xff000000))
        return;
    // we create a 1x1 black pixel image that is added to display just behind video
    DISPMANX_DISPLAY_HANDLE_T display;
    DISPMANX_UPDATE_HANDLE_T update;
    DISPMANX_RESOURCE_HANDLE_T resource;
    DISPMANX_ELEMENT_HANDLE_T element;
    int ret;
    uint32_t vc_image_ptr;
    VC_IMAGE_TYPE_T type = VC_IMAGE_ARGB8888;
    int layer = m_config_video.layer - 1;

    VC_RECT_T dst_rect, src_rect;

    display = vc_dispmanx_display_open(m_config_video.display);
    assert(display);

    resource = vc_dispmanx_resource_create(type, 1 /*width*/, 1 /*height*/, &vc_image_ptr);
    assert(resource);

    vc_dispmanx_rect_set(&dst_rect, 0, 0, 1, 1);

    ret = vc_dispmanx_resource_write_data(resource, type, sizeof(rgba), &rgba, &dst_rect);
    assert(ret == 0);

    vc_dispmanx_rect_set(&src_rect, 0, 0, 1 << 16, 1 << 16);
    vc_dispmanx_rect_set(&dst_rect, 0, 0, 0, 0);

    update = vc_dispmanx_update_start(0);
    assert(update);

    element = vc_dispmanx_element_add(update, display, layer, &dst_rect, resource, &src_rect,
                                      DISPMANX_PROTECTION_NONE, NULL, NULL, DISPMANX_STEREOSCOPIC_MONO);
    assert(element);

    ret = vc_dispmanx_update_submit_sync(update);
    assert(ret == 0);
}

// bool OMXPlayerImpl::OpenFile(const std::string& filename)
// {

//     return false;
// }

bool OMXPlayerImpl::LoadFile(const std::string &filename)
{
    float m_threshold = -1.0f; // amount of audio/video required to come out of buffering
    float m_timeout = 10.0f;   // amount of time file/network operation can stall for before timing out
    bool m_dump_format = false;
    std::string m_cookie = "";
    std::string m_user_agent = "";
    std::string m_lavfdopts = "";
    std::string m_avdict = "";
    FORMAT_3D_T m_3d = CONF_FLAGS_FORMAT_NONE;
    int m_orientation = -1; // unset
    float m_fps = 0.0f;     // unset
    TV_DISPLAY_STATE_T tv_state;

    FlushStreams(DVD_NOPTS_VALUE);

    bool filename_is_URL = IsURL(filename);
    if (!filename_is_URL && !IsPipe(filename) && !Exists(filename))
    {
        std::cout << "File not found: " << filename << std::endl;
        return false;
    }

    bool m_audio_extension = false;
    const CStdString m_musicExtensions = ".nsv|.m4a|.flac|.aac|.strm|.pls|.rm|.rma|.mpa|.wav|.wma|.ogg|.mp3|.mp2|.m3u|.mod|.amf|.669|.dmf|.dsm|.far|.gdm|"
                                         ".imf|.it|.m15|.med|.okt|.s3m|.stm|.sfx|.ult|.uni|.xm|.sid|.ac3|.dts|.cue|.aif|.aiff|.wpl|.ape|.mac|.mpc|.mp+|.mpp|.shn|.zip|.rar|"
                                         ".wv|.nsf|.spc|.gym|.adx|.dsp|.adp|.ymf|.ast|.afc|.hps|.xsp|.xwav|.waa|.wvs|.wam|.gcm|.idsp|.mpdsp|.mss|.spt|.rsd|.mid|.kar|.sap|"
                                         ".cmc|.cmr|.dmc|.mpt|.mpd|.rmt|.tmc|.tm8|.tm2|.oga|.url|.pxml|.tta|.rss|.cm3|.cms|.dlt|.brstm|.mka";
    if (filename.find_last_of(".") != string::npos)
    {
        CStdString extension = filename.substr(filename.find_last_of("."));
        if (!extension.IsEmpty() && m_musicExtensions.Find(extension.ToLower()) != -1)
            m_audio_extension = true;
    }

    if (!m_omx_reader.Open(filename.c_str(), m_dump_format, m_config_audio.is_live, m_timeout, m_cookie.c_str(), m_user_agent.c_str(), m_lavfdopts.c_str(), m_avdict.c_str()))
        return false;

    m_has_video = m_omx_reader.VideoStreamCount();
    m_has_audio = m_audio_index_use < 0 ? false : m_omx_reader.AudioStreamCount();

    m_has_video = m_omx_reader.VideoStreamCount();
    m_has_audio = m_audio_index_use < 0 ? false : m_omx_reader.AudioStreamCount();
    m_loop = m_loop && m_omx_reader.CanSeek();

    if (m_audio_extension)
    {
        // CLog::Log(LOGWARNING, "%s - Ignoring video in audio filetype:%s", __FUNCTION__, filename.c_str());
        m_has_video = false;
    }

    if (filename.find("3DSBS") != string::npos || filename.find("HSBS") != string::npos)
        m_3d = CONF_FLAGS_FORMAT_SBS;
    else if (filename.find("3DTAB") != string::npos || filename.find("HTAB") != string::npos)
        m_3d = CONF_FLAGS_FORMAT_TB;

    // 3d modes don't work without switch hdmi mode
    if (m_3d != CONF_FLAGS_FORMAT_NONE || m_NativeDeinterlace)
        m_refresh = true;

    // you really don't want want to match refresh rate without hdmi clock sync
    if ((m_refresh || m_NativeDeinterlace) && !m_no_hdmi_clock_sync)
        m_config_video.hdmi_clock_sync = true;

    if (!m_av_clock->OMXInitialize())
        return false;

    if (m_config_video.hdmi_clock_sync && !m_av_clock->HDMIClockSync())
        return false;

    m_av_clock->OMXStateIdle();
    m_av_clock->OMXStop();
    m_av_clock->OMXPause();

    m_omx_reader.GetHints(OMXSTREAM_AUDIO, m_config_audio.hints);
    m_omx_reader.GetHints(OMXSTREAM_VIDEO, m_config_video.hints);

    if (m_fps > 0.0f)
        m_config_video.hints.fpsrate = m_fps * DVD_TIME_BASE, m_config_video.hints.fpsscale = DVD_TIME_BASE;

    if (m_audio_index_use > 0)
        m_omx_reader.SetActiveStream(OMXSTREAM_AUDIO, m_audio_index_use - 1);

    if (m_has_video && m_refresh)
    {
        memset(&tv_state, 0, sizeof(TV_DISPLAY_STATE_T));
        m_BcmHost.vc_tv_get_display_state(&tv_state);

        SetVideoMode(m_config_video.hints.width, m_config_video.hints.height, m_config_video.hints.fpsrate, m_config_video.hints.fpsscale, m_3d);
    }
    // get display aspect
    TV_DISPLAY_STATE_T current_tv_state;
    memset(&current_tv_state, 0, sizeof(TV_DISPLAY_STATE_T));
    m_BcmHost.vc_tv_get_display_state(&current_tv_state);
    if (current_tv_state.state & (VC_HDMI_HDMI | VC_HDMI_DVI))
    {
        //HDMI or DVI on
        m_config_video.display_aspect = GetDisplayAspectRatio((HDMI_ASPECT_T)current_tv_state.display.hdmi.aspect_ratio);
    }
    else
    {
        //composite on
        m_config_video.display_aspect = GetDisplayAspectRatio((SDTV_ASPECT_T)current_tv_state.display.sdtv.display_options.aspect);
    }
    m_config_video.display_aspect *= (float)current_tv_state.display.hdmi.height / (float)current_tv_state.display.hdmi.width;

    if (m_orientation >= 0)
        m_config_video.hints.orientation = m_orientation;
    if (m_has_video && !m_player_video.Open(m_av_clock, m_config_video))
        return false;

    m_omx_reader.GetHints(OMXSTREAM_AUDIO, m_config_audio.hints);

    if (m_config_audio.device == "")
    {
        if (m_BcmHost.vc_tv_hdmi_audio_supported(EDID_AudioFormat_ePCM, 2, EDID_AudioSampleRate_e44KHz, EDID_AudioSampleSize_16bit) == 0)
            m_config_audio.device = "omx:hdmi";
        else
            m_config_audio.device = "omx:local";
    }

    if (m_config_audio.device == "omx:alsa" && m_config_audio.subdevice.empty())
        m_config_audio.subdevice = "default";

    if ((m_config_audio.hints.codec == AV_CODEC_ID_AC3 || m_config_audio.hints.codec == AV_CODEC_ID_EAC3) &&
        m_BcmHost.vc_tv_hdmi_audio_supported(EDID_AudioFormat_eAC3, 2, EDID_AudioSampleRate_e44KHz, EDID_AudioSampleSize_16bit) != 0)
        m_config_audio.passthrough = false;
    if (m_config_audio.hints.codec == AV_CODEC_ID_DTS &&
        m_BcmHost.vc_tv_hdmi_audio_supported(EDID_AudioFormat_eDTS, 2, EDID_AudioSampleRate_e44KHz, EDID_AudioSampleSize_16bit) != 0)
        m_config_audio.passthrough = false;

    if (m_has_audio && !m_player_audio.Open(m_av_clock, m_config_audio, &m_omx_reader))
        return false;

    if (m_has_audio)
    {
        m_player_audio.SetVolume(pow(10, m_Volume / 2000.0));
        if (m_Amplification)
            m_player_audio.SetDynamicRangeCompression(m_Amplification);
    }

    if (m_threshold < 0.0f)
        m_threshold = m_config_audio.is_live ? 0.7f : 0.2f;

    m_av_clock->OMXReset(m_has_video, m_has_audio);

    return true;
}

void OMXPlayerImpl::Process()
{
    bool m_send_eos = false;
    double m_incr = 0;
    double m_loop_from = 0;
    bool m_refresh = false;
    double startpts = 0;
    float m_threshold = -1.0f; // amount of audio/video required to come out of buffering
    // TV_DISPLAY_STATE_T tv_state;
    double last_seek_pos = 0;
    bool m_seek_flush = false;
    bool m_Pause = false;
    std::string m_cookie = "";
    std::string m_user_agent = "";
    std::string m_lavfdopts = "";
    std::string m_avdict = "";
    double m_last_check_time = 0.0;
    std::string mode;
    ///////////////////////////////////////////////////////////////

    // if (!LoadFile("/mnt/userdata/resources/video/intro_video.mp4"))
    //     goto do_exit;
    m_player_state = OMXPlayerState::IDLE;

    if (m_loop)
        m_loop_from = m_incr;

    while (!m_stop)
    {

        double now = m_av_clock->GetAbsoluteClock();
        bool update = false;
        if (m_last_check_time == 0.0 || m_last_check_time + DVD_MSEC_TO_TIME(20) <= now)
        {
            update = true;
            m_last_check_time = now;
        }

        OMXPlayerCommand cmd;

        if (m_command_queue.try_pop(cmd))
        {
            switch (cmd.action)
            {
            case OMXPlayerAction::OPEN_FILE:
            {
                if (LoadFile(cmd.filename))
                {
                    m_player_state = OMXPlayerState::READY;
                }
                else
                {
                    m_player_state = OMXPlayerState::IDLE;
                }
                break;
            }
            case OMXPlayerAction::PLAY:
            {
                if (m_player_state == OMXPlayerState::PAUSED)
                {
                    m_Pause = false;
                }
                else
                {
                    if (m_player_state == OMXPlayerState::READY)
                    {
                        m_av_clock->OMXReset(m_has_video, m_has_audio);
                        m_av_clock->OMXStateExecute();
                    }
                    else
                    {
                    }
                    m_player_state = OMXPlayerState::PLAYING;
                }
                break;
            }
            case OMXPlayerAction::PAUSE:
            {
                m_Pause = true;
                m_player_state = OMXPlayerState::PAUSED;
                break;
            }
            case OMXPlayerAction::STOP:
            {
                Cleanup();
                m_player_state = OMXPlayerState::IDLE;
                break;
            }
            case OMXPlayerAction::SEEK_TO:
            {
                double oldPos, newPos;
                auto seek_to = cmd.iarg[0];
                std::cout << "Seek to: " << seek_to << std::endl;

                newPos = seek_to * 1e-6;
                oldPos = m_av_clock->OMXMediaTime() * 1e-6;
                m_incr = newPos - oldPos;

                break;
            }
            default:;
            }
        }

        if (m_player_state == OMXPlayerState::IDLE || m_player_state == OMXPlayerState::READY)
        {
            usleep(10000);
            continue;
        }

        if (m_seek_flush || m_incr != 0)
        {
            double seek_pos = 0;
            double pts = 0;

            pts = m_av_clock->OMXMediaTime();

            seek_pos = (pts ? pts / DVD_TIME_BASE : last_seek_pos) + m_incr;
            last_seek_pos = seek_pos;

            seek_pos *= 1000.0;

            if (m_omx_reader.SeekTime((int)seek_pos, m_incr < 0.0f, &startpts))
            {
                FlushStreams(startpts);
            }

            if (m_omx_reader.IsEof())
                m_player_state = OMXPlayerState::IDLE;

            // Quick reset to reduce delay during loop & seek.
            if (m_has_video && !m_player_video.Reset())
                m_player_state = OMXPlayerState::IDLE;

            CLog::Log(LOGDEBUG, "Seeked %.0f %.0f %.0f\n", DVD_MSEC_TO_TIME(seek_pos), startpts, m_av_clock->OMXMediaTime());

            m_av_clock->OMXPause();
            m_av_clock->OMXReset(m_has_video, m_has_audio);

            m_seek_flush = false;
            m_incr = 0;
        }

        /* player got in an error state */
        if (m_player_audio.Error())
        {
            printf("audio player error. emergency exit!!!\n");
            goto do_exit;
        }

        if (update)
        {
            /* when the video/audio fifos are low, we pause clock, when high we resume */
            double stamp = m_av_clock->OMXMediaTime();
            double audio_pts = m_player_audio.GetCurrentPTS();
            double video_pts = m_player_video.GetCurrentPTS();

            if (0 && m_av_clock->OMXIsPaused())
            {
                double old_stamp = stamp;
                if (audio_pts != DVD_NOPTS_VALUE && (stamp == 0 || audio_pts < stamp))
                    stamp = audio_pts;
                if (video_pts != DVD_NOPTS_VALUE && (stamp == 0 || video_pts < stamp))
                    stamp = video_pts;
                if (old_stamp != stamp)
                {
                    m_av_clock->OMXMediaTime(stamp);
                    stamp = m_av_clock->OMXMediaTime();
                }
            }

            float audio_fifo = audio_pts == DVD_NOPTS_VALUE ? 0.0f : audio_pts / DVD_TIME_BASE - stamp * 1e-6;
            float video_fifo = video_pts == DVD_NOPTS_VALUE ? 0.0f : video_pts / DVD_TIME_BASE - stamp * 1e-6;
            float threshold = std::min(0.1f, (float)m_player_audio.GetCacheTotal() * 0.1f);
            bool audio_fifo_low = false, video_fifo_low = false, audio_fifo_high = false, video_fifo_high = false;

            if (audio_pts != DVD_NOPTS_VALUE)
            {
                audio_fifo_low = m_has_audio && audio_fifo < threshold;
                audio_fifo_high = !m_has_audio || (audio_pts != DVD_NOPTS_VALUE && audio_fifo > m_threshold);
            }
            if (video_pts != DVD_NOPTS_VALUE)
            {
                video_fifo_low = m_has_video && video_fifo < threshold;
                video_fifo_high = !m_has_video || (video_pts != DVD_NOPTS_VALUE && video_fifo > m_threshold);
            }

            if (!m_Pause && (m_omx_reader.IsEof() || m_omx_pkt || TRICKPLAY(m_av_clock->OMXPlaySpeed()) || (audio_fifo_high && video_fifo_high)))
            {
                if (m_av_clock->OMXIsPaused())
                {
                    CLog::Log(LOGDEBUG, "Resume %.2f,%.2f (%d,%d,%d,%d) EOF:%d PKT:%p\n", audio_fifo, video_fifo, audio_fifo_low, video_fifo_low, audio_fifo_high, video_fifo_high, m_omx_reader.IsEof(), m_omx_pkt);
                    m_av_clock->OMXResume();
                }
            }
            else if (m_Pause || audio_fifo_low || video_fifo_low)
            {
                if (!m_av_clock->OMXIsPaused())
                {
                    if (!m_Pause)
                        m_threshold = std::min(2.0f * m_threshold, 16.0f);
                    CLog::Log(LOGDEBUG, "Pause %.2f,%.2f (%d,%d,%d,%d) %.2f\n", audio_fifo, video_fifo, audio_fifo_low, video_fifo_low, audio_fifo_high, video_fifo_high, m_threshold);
                    m_av_clock->OMXPause();
                }
            }
        }

        if (!m_omx_pkt)
            m_omx_pkt = m_omx_reader.Read();

        if (m_omx_pkt)
            m_send_eos = false;

        if (m_omx_reader.IsEof() && !m_omx_pkt)
        {
            if (!m_send_eos && m_has_video)
                m_player_video.SubmitEOS();
            if (!m_send_eos && m_has_audio)
                m_player_audio.SubmitEOS();
            m_send_eos = true;
            if ((m_has_video && !m_player_video.IsEOS()) ||
                (m_has_audio && !m_player_audio.IsEOS()))
            {
                OMXClock::OMXSleep(10);
                continue;
            }

            if (m_loop)
            {
                m_incr = m_loop_from - (m_av_clock->OMXMediaTime() ? m_av_clock->OMXMediaTime() / DVD_TIME_BASE : last_seek_pos);
                continue;
            }

            break;
        }

        if (m_has_video && m_omx_pkt && m_omx_reader.IsActive(OMXSTREAM_VIDEO, m_omx_pkt->stream_index))
        {
            // if (TRICKPLAY(m_av_clock->OMXPlaySpeed()))
            // {
            //     m_packet_after_seek = true;
            // }
            if (m_player_video.AddPacket(m_omx_pkt))
                m_omx_pkt = NULL;
            else
                OMXClock::OMXSleep(10);
        }
        else if (m_has_audio && m_omx_pkt && !TRICKPLAY(m_av_clock->OMXPlaySpeed()) && m_omx_pkt->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            if (m_player_audio.AddPacket(m_omx_pkt))
                m_omx_pkt = NULL;
            else
                OMXClock::OMXSleep(10);
        }
        else
        {
            if (m_omx_pkt)
            {
                m_omx_reader.FreePacket(m_omx_pkt);
                m_omx_pkt = NULL;
            }
            else
                OMXClock::OMXSleep(10);
        }
    }

do_exit:
    Cleanup();
}

void OMXPlayerImpl::Cleanup()
{
    if (m_NativeDeinterlace)
    {
        char response[80];
        vc_gencmd(response, sizeof response, "hvs_update_fields %d", 0);
    }
    if (m_has_video && m_refresh && tv_state.display.hdmi.group && tv_state.display.hdmi.mode)
    {
        m_BcmHost.vc_tv_hdmi_power_on_explicit_new(HDMI_MODE_HDMI, (HDMI_RES_GROUP_T)tv_state.display.hdmi.group, tv_state.display.hdmi.mode);
    }

    m_av_clock->OMXStop();
    m_av_clock->OMXStateIdle();

    m_player_video.Close();
    m_player_audio.Close();

    if (m_omx_pkt)
    {
        m_omx_reader.FreePacket(m_omx_pkt);
        m_omx_pkt = NULL;
    }

    m_omx_reader.Close();

    m_av_clock->OMXDeinitialize();
    if (m_av_clock)
        delete m_av_clock;

    vc_tv_show_info(0);
}

bool OMXPlayerImpl::OpenFile(const std::string &filename)
{
    // "/mnt/userdata/resources/video/intro_video.mp4"
    m_command_queue.push({OMXPlayerAction::OPEN_FILE,
                          {0, 0, 0, 0},
                          filename});
    return true;
}

bool OMXPlayerImpl::Play()
{
    m_command_queue.push({OMXPlayerAction::PLAY});
    return true;
}

bool OMXPlayerImpl::Stop()
{
    // m_command_queue.push({OMXPlayerAction::SEEK_TO, {0}});
    // m_command_queue.push({OMXPlayerAction::PAUSE});
    m_command_queue.push({OMXPlayerAction::STOP});
    return true;
}

bool OMXPlayerImpl::Pause()
{
    m_command_queue.push({OMXPlayerAction::PAUSE});
    return true;
}

bool OMXPlayerImpl::SeekTo(int pos)
{
    m_command_queue.push({OMXPlayerAction::SEEK_TO, {pos}});
    return true;
}

bool OMXPlayerImpl::SeekBy(int pos)
{
    return true;
}