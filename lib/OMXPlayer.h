#ifndef _OMX_PLAYER_H_
#define _OMX_PLAYER_H_

#include <string>

class OMXPlayerImpl;

class OMXPlayer
{
public:
    OMXPlayer();
    ~OMXPlayer();

    bool OpenFile(const std::string& filename);
    bool Play();
    bool Stop();
    bool Pause();
    bool SeekTo(int pos);
    bool SeekBy(int pos);

    void SetWindowRect(int x1, int y1, int x2, int y2);
    void SetCropRect(int x1, int y1, int x2, int y2);

    void SetLayer(unsigned layer);
    void SetAlpha(unsigned alpha);

private:
    OMXPlayerImpl *m_impl;
};

#endif /* _OMX_PLAYER_H_ */