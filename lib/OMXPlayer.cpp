#include "OMXPlayer.h"
#include "OMXPlayerImpl.h"

OMXPlayer::OMXPlayer()
{
    m_impl = new OMXPlayerImpl();
}

OMXPlayer::~OMXPlayer()
{
    delete m_impl;
}

void OMXPlayer::SetWindowRect(int x1, int y1, int x2, int y2)
{
    m_impl->SetWindowRect(x1, y1, x2, y2);
}

void OMXPlayer::SetCropRect(int x1, int y1, int x2, int y2)
{
    m_impl->SetCropRect(x1, y1, x2, y2);
}

void OMXPlayer::SetLayer(unsigned layer)
{
    m_impl->SetLayer(layer);
}

void OMXPlayer::SetAlpha(unsigned alpha)
{
    m_impl->SetAlpha(alpha);
}


bool OMXPlayer::OpenFile(const std::string& filename)
{
    m_impl->OpenFile(filename);
}

bool OMXPlayer::Play()
{
    m_impl->Play();
}

bool OMXPlayer::Stop()
{
    m_impl->Stop();
}

bool OMXPlayer::Pause()
{
    m_impl->Pause();
}

bool OMXPlayer::SeekTo(int pos)
{
    m_impl->SeekTo(pos);
}

bool OMXPlayer::SeekBy(int pos)
{
    m_impl->SeekBy(pos);
}

