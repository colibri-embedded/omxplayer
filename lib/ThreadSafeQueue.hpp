#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

namespace thread_safe {

template <typename T, unsigned SIZE = 8>
class Queue {
public:
    bool empty()
    {
        std::lock_guard<std::mutex> mlock(mutex_);
        return queue_.empty();
    }

    bool full()
    {
        std::lock_guard<std::mutex> mlock(mutex_);
        return _full;
    }

    bool _full()
    {
        if (SIZE == 0)
            return false;

        return queue_.size() >= SIZE;
    }

    unsigned size()
    {
        std::lock_guard<std::mutex> mlock(mutex_);
        return queue_.size();
    }

    T pop()
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (queue_.empty()) {
            cond_.wait(mlock);
        }
        auto item = queue_.front();
        queue_.pop();
        return item;
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (queue_.empty()) {
            cond_.wait(mlock);
        }
        item = queue_.front();
        queue_.pop();
        mlock.unlock();
        cond_.notify_one();
    }

    bool try_pop(T& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        if (queue_.empty())
            return false;
        item = queue_.front();
        queue_.pop();
        mlock.unlock();
        cond_.notify_one();
        return true;
    }

    void push(const T& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (_full()) {
            cond_.wait(mlock);
        }
        queue_.push(item);
        mlock.unlock();
        cond_.notify_one();
    }

    void push(T&& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        while (_full()) {
            cond_.wait(mlock);
        }
        queue_.push(std::move(item));
        mlock.unlock();
        cond_.notify_one();
    }

    bool try_push(const T& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        if (_full())
            return false;
        queue_.push(item);
        mlock.unlock();
        cond_.notify_one();
        return true;
    }

    bool try_push(T&& item)
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        if (_full())
            return false;
        queue_.push(std::move(item));
        mlock.unlock();
        cond_.notify_one();
        return true;
    }

    void clear()
    {
        std::unique_lock<std::mutex> mlock(mutex_);
        queue_ = {};
        mlock.unlock();
        cond_.notify_one();
    }

private:
    std::queue<T> queue_;
    std::mutex mutex_;
    std::condition_variable cond_;
};

} // namespace fabui

#endif /* THREAD_SAFE_QUEUE_HPP */
