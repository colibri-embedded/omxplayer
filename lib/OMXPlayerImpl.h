#ifndef _OMX_PLAYER_IMPL_H_
#define _OMX_PLAYER_IMPL_H_

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <termios.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <string.h>

#define AV_NOWARN_DEPRECATED

extern "C"
{
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
};

#include "revision.h"
#include "OMXStreamInfo.h"

#include "utils/log.h"

#include "DllAvUtil.h"
#include "DllAvFormat.h"
#include "DllAvCodec.h"
#include "linux/RBP.h"

#include "OMXVideo.h"
#include "OMXAudioCodecOMX.h"
#include "utils/PCMRemap.h"
#include "OMXClock.h"
#include "OMXAudio.h"
#include "OMXReader.h"
#include "OMXPlayerVideo.h"
#include "OMXPlayerAudio.h"
#include "OMXThread.h"
#include "DllOMX.h"
#include "Srt.h"
#include "utils/Strprintf.h"

#include "ThreadSafeQueue.hpp"

#include <string>
#include <utility>
#include <memory>

typedef enum
{
  CONF_FLAGS_FORMAT_NONE,
  CONF_FLAGS_FORMAT_SBS,
  CONF_FLAGS_FORMAT_TB,
  CONF_FLAGS_FORMAT_FP
} FORMAT_3D_T;

enum class OMXPlayerAction {
    NONE = 0,
    OPEN_FILE,
    DECREASE_SPEED,
    INCREASE_SPEED,
    REWIND,
    FORWARD,
    STEP,
    PREVIOUS_AUDIO,
    NEXT_AUDIO,
    //PREVIOUS_CHAPTER,
    //NEXT_CHAPTER,
    PREVIOUS_SUBTITLE,
    NEXT_SUBTITLE,
    TOGGLE_SUBTITLE,
    HIDE_SUBTITLES,
    SHOW_SUBTITLES,
    DECREASE_SUBTITLE_DELAY,
    INCREASE_SUBTITLE_DELAY,

    SEEK_BACK, // amount, relative|absolute
    SEEK_FORWARD,  // amount, relative|absolute

    SEEK_TO,

    SET_ALPHA,
    SET_LAYER,

    PLAY,
    STOP,
    PAUSE,
    RESUME,

    MOVE_VIDEO,
    CROP_VIDEO,

    HIDE_VIDEO,
    UNHIDE_VIDEO,

    SET_ASPECT_MODE,

    DECREASE_VOLUME,
    INCREASE_VOLUME
};

enum class OMXPlayerState {
    IDLE = 0,
    READY,
    PLAYING,
    PAUSED
};

struct OMXPlayerCommand {
    OMXPlayerAction action = OMXPlayerAction::NONE;
    int iarg[4] = {0,0,0,0};
    std::string filename = "";
};

class OMXPlayerImpl : public OMXThread
{
public:
    OMXPlayerImpl();
    virtual ~OMXPlayerImpl();

    bool OpenFile(const std::string& filename);

    bool Play();
    bool Stop();
    bool Pause();
    bool SeekTo(int pos);
    bool SeekBy(int pos);

    void SetWindowRect(int x1, int y1, int x2, int y2);
    void SetCropRect(int x1, int y1, int x2, int y2);

    void SetLayer(unsigned layer);
    void SetAlpha(unsigned alpha);

protected:
    thread_safe::Queue<OMXPlayerCommand> m_command_queue;
    OMXPlayerState m_player_state = OMXPlayerState::IDLE;

    std::string m_filename = "";

    enum PCMChannels *m_pChannelMap = NULL;
    long m_Volume = 0;
    long m_Amplification = 0;
    bool m_NativeDeinterlace = false;
    bool m_HWDecode = false;
    // bool m_osd = !is_model_pi4() && !is_fkms_active();

    bool m_centered = false;
    bool m_ghost_box = true;

    int m_audio_index_use = 0;
    bool m_no_hdmi_clock_sync = false;
    bool m_stop = false;
    int m_subtitle_index = -1;

    TV_DISPLAY_STATE_T tv_state;
    bool m_refresh = false;
    int m_tv_show_info = 0;
    bool m_has_video = false;
    bool m_has_audio = false;

    bool m_loop = false;

    CRBP g_RBP;
    COMXCore g_OMX;
    OMXReader m_omx_reader;
    OMXVideoConfig m_config_video;
    OMXPlayerVideo m_player_video;
    OMXAudioConfig m_config_audio;
    OMXPlayerAudio m_player_audio;
    OMXClock *m_av_clock = NULL;
    OMXPacket *m_omx_pkt = NULL;
    DllBcmHost m_BcmHost;

    bool Open();
    bool Close();
    void Process();

    bool LoadFile(const std::string& filename);
    void Cleanup();

    void FlushStreams(double pts);
    void SetSpeed(int iSpeed);
    float GetDisplayAspectRatio(SDTV_ASPECT_T aspect);
    float GetDisplayAspectRatio(HDMI_ASPECT_T aspect);
    void SetVideoMode(int width, int height, int fpsrate, int fpsscale, FORMAT_3D_T is3d);
    bool IsURL(const std::string &str);
    bool Exists(const std::string &path);
    bool IsPipe(const std::string &str);
    int GetGPUMemory(void);
    void SetBackground(uint32_t rgba);

};

#endif /* _OMX_PLAYER_IMPL_H_ */